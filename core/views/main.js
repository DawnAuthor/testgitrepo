define(["tpl!templates/main.htm", "views/navbar", "views/footer", "css!css/bootstrap.css", "css!css/global.css"], function(template, Navbar, Footer){
	var MainView = Backbone.View.extend({
		initialize: function(){
			var self = this, navbar, footer;
			navbar = new Navbar().render();
			footer = new Footer().render();
		},
		el: "#content",
		render: function(){
			var self = this;
			//render main login view template
			self.$el.empty();
			self.$el.html(template);
		}
});
return MainView;
});
