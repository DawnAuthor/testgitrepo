define(['tpl!templates/users.template.htm', 'collections/UsersCollection', 'css!css/bootstrap.css'],

function(template, UsersCollection){

	var UsersView = Backbone.View.extend({

		el:'#content',

		render: function(){
			var self = this, users;

			users = new UsersCollection();
			users.fetch().done(function(){
				self.$el.html(template({users: users}));
			});
		}
	});

	return UsersView;
});