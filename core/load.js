requirejs.config({
     baseUrl: "core",
     paths: {
          views: "views",
          models: "models",
          collections: "collections",
          templates: "templates",
          css: "css",
          tpl: "tpl"
     }
});