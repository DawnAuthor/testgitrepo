define([], function(){
	
	var User = Backbone.Model.extend({
		defaults:{
			user_id: "",
			user_name: "",
			user_title: "",
			user_email: ""
		}, 
		idAttribute: "user_id",
		urlRoot: "http://localhost/barebones/V1/users/accounts"
	});

	return User;
});

