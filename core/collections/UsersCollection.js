define(['models/User'], function(User){
	
	var UsersCollection = Backbone.Collection.extend({
		model: User,
		url: "http://localhost/barebones/V1/users/accounts",
		parse: function(response){
			return response.data;
		}
	});

	return UsersCollection;
});